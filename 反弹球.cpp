#include "pch.h"
#pragma warning(disable:4996)
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

// 全局变量
int high, width;   //  游戏画面尺寸
int ball_x, ball_y;  // 小球的坐标
int ball_vx, ball_vy;   // 小球的速度
int position_x, position_y;  // 挡板中心坐标
int radius;        //挡板的半径大小
int left, right;    //挡板的左右边界
int ball_number = 0;  //反弹的次数
int mubiao_x1, mubiao_y1;//障碍物坐标
int mubiao_x2, mubiao_y2;//障碍物坐标
int mubiao_x3, mubiao_y3;//障碍物坐标
int mubiao_x4, mubiao_y4;//障碍物坐标
int mubiao_x5, mubiao_y5;//障碍物坐标
int score;         //击中目标得分
void HideCursor()   
{
	CONSOLE_CURSOR_INFO cursor_info = { 1, 0 };
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
}
void gotoxy(int x, int y)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle, pos);
}

void startup() // 数据初始化
{
	high = 16;
	width = 25;
	ball_x = 1;
	ball_y = width / 2;
	ball_vx = 1;
	ball_vy = 1;
	position_x = high;
	position_y = width / 2;
	radius = 5;
	left = position_y - radius;
	right = position_y + radius;
	mubiao_x1 = 0;
	mubiao_y1 = position_y-1;
	mubiao_x2 = 2;
	mubiao_y2 = 6;
	mubiao_x3 = 4;
	mubiao_y3 = 1;
	mubiao_x4 = 5;
	mubiao_y4 = 10;
	mubiao_x4 = 10;
	mubiao_y4 = 7;
	mubiao_x5 = 3;
	mubiao_y5 = 11;
	score = 0;
	HideCursor();      //隐藏光标
}

void show()  // 显示画面
{
	gotoxy(0, 0);// 光标移动到原点位置进行重画清屏
	HideCursor();
	int i, j;
	for (i = 0; i <= high; i++)
	{
		for (j = 0; j <= width; j++)
		{
			if ((i == ball_x) && (j == ball_y))
				printf("o");  //   输出小球o
			else if (j == width)
				printf("|"); //输出右边界
			else if (i == high + 1)
				printf("-"); //输出下边界
			else if ((i == high) && (j >= left) && (j <= right))
				printf("*");  //输出挡板
			else if ((i == mubiao_x1) && (j == mubiao_y1))
				printf("B");  //输出目标
			else if ((i == mubiao_x2) && (j == mubiao_y2))
				printf("B");  //输出目标
			else if ((i == mubiao_x3) && (j == mubiao_y3))
				printf("A");  //输出目标
			else if ((i == mubiao_x4) && (j == mubiao_y4))
				printf("B");  //输出目标
			else if ((i == mubiao_x5) && (j == mubiao_y5))
				printf("D");  //输出目标
			else
				printf(" "); //输出空格
		}
		printf("\n");
	}
	printf("反弹小球数：%d\n", ball_number);
	printf("score:%d\n", score);
}

void updateWithoutInput()  // 与用户输入无关的更新
{
	if ((ball_x == mubiao_x1) && (ball_y == mubiao_y1))
	{
		score++;
		mubiao_x1 = rand() % (high - 3);
		mubiao_y1 = rand() % width;
	}
	if ((ball_x == mubiao_x2) && (ball_y == mubiao_y2))
	{
		score++;
		mubiao_x2 = rand() % (high - 3);
		mubiao_y2 = rand() % width;
	}
	if ((ball_x == mubiao_x3) && (ball_y == mubiao_y3))
	{
		score += 2;
		mubiao_x3 = rand() % (high - 3);
		mubiao_y3 = rand() % width;
	}
	if ((ball_x == mubiao_x4) && (ball_y == mubiao_y4))
	{
		score++;
		mubiao_x4 = rand() % (high - 3);
		mubiao_y4 = rand() % width;
	}
	if ((ball_x == mubiao_x5) && (ball_y == mubiao_y5))
	{
		printf("Game Over!");
		exit(0);
	}
	if (ball_x == high-1)
	{
		if ((ball_y >= left) && (ball_y <= right))
		{
			ball_number++;
			ball_vx = -ball_vx;
		}
		if (ball_x == high - 1 && ball_y<position_y - radius || ball_x == high - 1 && ball_y>position_y + radius)
		{
			printf("Game Over!");
			exit(0);
		}
	}
	//根据速度更新小球位置
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;
	//碰到边界，改变速度方向,实现反弹
	if(ball_x==0)
		ball_vx=-ball_vx;
	if ((ball_y <= 0) || (ball_y >= width - 1))
		ball_vy = -ball_vy;
	Sleep(100);
}

void updateWithInput()  // 与用户输入有关的更新
{
	char input;
	if (kbhit())  // 判断是否有输入
	{
		input = getch();  // 根据用户的不同输入来移动，不必输入回车
		if (input == 'a')
		{
			position_y--;  // 位置左移
			left = position_y - radius;
			right = position_y + radius;
			if ((ball_x == position_x - 1) && (ball_y >= left) && (ball_y <= right) && ball_vy >= -3 && ball_vy <= 3)
				ball_vy = ball_vy - 1;
		}
		if (input == 'd')
		{
			position_y++;  // 位置右移;
			left = position_y - radius;
			right = position_y + radius;
			if ((ball_x == position_x - 1) && (ball_y >= left) && (ball_y <= right) && ball_vy >= -3 && ball_vy <= 3)
				ball_vy = ball_vy + 1;
		}
	}
}

void main()
{
	startup();  // 数据初始化	
	while (1) //  游戏循环执行
	{
		show();  // 显示画面
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();  // 与用户输入有关的更新
	}
}